# Game Jam Isima 16/12/2018
Game Jam Isima (1ère année)
Thème : "Fuite"
Codé en C++ avec [SFML](https://www.sfml-dev.org/ "Simple and Fast Multimedia Library") et [rapidjson](http://rapidjson.org/ "JSON Parser")

---
Mathieu Arquillière - Jérémy Zangla - Louis Turpinat - Décembre 2018